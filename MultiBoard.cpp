#include "MultiBoard.h"

MultiBoard::MultiBoard(bool b) {
    //init variable 
    isFinalNode = b;

    for(int i = 0 ; i < MAX_ACTIONS_NUM ; i++)
        actions[i] = NULL_ACTION;
    for(int i = 0 ; i < MAX_ACTIONS_NUM ; i++)
        doActions[i] = NULL;

    commandStrPointer = 0;
}

void MultiBoard::begin(SoftwareSerial *s) {
    //get software serial and serial object
    sSerial = s;
    sSerial->begin(9600);
    Serial.begin(9600);
}

void MultiBoard::run() {
    //read feedback from software serial
    if(sSerial->available())
        Serial.print((char) sSerial->read());

    //read command from serial
    if(commandStrPointer < COMMAND_BUF){
        if(Serial.available()){
            //read char from serial stream 
            //and check whether it is the end of the command
            if((command[commandStrPointer] = Serial.read()) == COMMAND_END_SIGN){
                command[commandStrPointer + 1] = '\0'; //put string final char
                commandStrPointer = 0; //reset commandStrPointer, ready for next command

                //analyze command
                int action;
                sscanf(command, "%d", &action);

                //find action in action list
                int i = 0;
                while (i < 10) {
                    if(actions[i] == action)
                        break;
                    i++;
                }

                //...and do action
                if (i != 10) {
                    Do doAction = doActions[i];
                    doAction(command);
                } else {
                    throwCommand();
                }
            } else {
                commandStrPointer++;
            }
        }
    }
}

bool MultiBoard::addAction(int action,Do doAction){
    //get last place
    int i = 0;
    while (i < 10) {
        if (actions[i] == NULL_ACTION)
            break;
        i++;
    }

    //check whether have enough space and set action
    if (i == 10) {
        return false;
    } else {
        actions[i] = action;
        doActions[i] = doAction;
    }
    return true;
}

void MultiBoard::throwCommand(){
    if(isFinalNode){
        doErr();
    } else {
        sSerial->print(command);
    }
}

void MultiBoard::doOk(){
    Serial.print(OK);
    Serial.print(COMMAND_END_SIGN);
}

void MultiBoard::doErr(){
    Serial.print(ERROR);
    Serial.print(COMMAND_END_SIGN);
}
