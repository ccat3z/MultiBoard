#include <Arduino.h>
#include <SoftwareSerial.h>

#ifndef MultiBoard_h
#define MultiBoard_h

#define COMMAND_BUF 50
#define COMMAND_END_SIGN '#'

#define OK 100
#define ERROR 200
#define NULL_ACTION 0

#define MAX_ACTIONS_NUM 10
typedef void (*Do)(char*);

class MultiBoard
{
    public:
        MultiBoard(bool b);
        void begin(SoftwareSerial *s);
        bool addAction(int action,Do doAction);
        void run();
        void doErr();
        void doOk();

    private:
        void throwCommand();

        SoftwareSerial *sSerial;
        char command[COMMAND_BUF];
        int commandStrPointer;
        bool isFinalNode;
        int actions[MAX_ACTIONS_NUM];
        Do doActions[MAX_ACTIONS_NUM];
};

#endif 
