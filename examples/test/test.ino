#include <MultiBoard.h>

MultiBoard md(true);
SoftwareSerial sSerial(10,11);

void doLed(char *);

void setup(){
    md.begin(&sSerial);
    md.addAction(10,doLed);
    pinMode(13,OUTPUT);
}

void loop(){
    digitalWrite(3,HIGH);
    digitalWrite(A0,HIGH);
    md.run();
}

void doLed(char command[COMMAND_BUF]){
    int action, ledAction;
    //analyze command argv
    sscanf(command, "%d %d", &action, &ledAction);
    if(ledAction == 1){
        digitalWrite(13,HIGH);
    } else {
        digitalWrite(13,LOW);
    }
    md.doOk();
}
